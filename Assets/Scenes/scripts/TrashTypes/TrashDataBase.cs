﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashDataBase
{
    public struct Recyclable_waste
    {
        public GameObject item;
        public string name;
        public bool Paper_waste;
        public bool Plastic_waste;
        public bool Metal_waste;
    }
    public struct General_waste
    {
        public GameObject item;
        public string name;
        public bool organic;
        public bool E_waste;
        public bool Textile;
    }

    public Recyclable_waste[] r_waste;
    public General_waste[] g_waste;

}



//****************Concept of using classes for the classification*/
//total number of trash items is 30
/*  public class Recycle_item
  {
      //paper
      //plastic
      //metal

      public GameObject[] Items;

/*&        //List of paper waste
      public class Paper_Recycling
      {
          public GameObject[] Items;
      }

      //List of plastic waste
      public class Plastic_Recycling
      {
          public GameObject[] Items;
      }

      public class Metal_Reclying
      {
          public GameObject[] Items;
      }

  }*/

/*    public class GeneralWaste_item
    {
        //organic
        //E-waste
        //Textiles

        //General list of all waste
        public GameObject[] Items;

        //List of Organic waste
        public class Organic_waste
        {
            public GameObject[] Items;
        }

        //List of all the E-waste
        public class E_Waste
        {
            public GameObject[] Items;
        }

        //List of all the Textiles waste
        public class Textile_waste
        {
            public GameObject[] Items;
        }
    }*/ 
