﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashAllocation:MonoBehaviour
{
    
    TrashDataBase DataBase=new TrashDataBase();
    int r_count=0;
    int g_count = 0;
    //function to allocate thee trash to its category;
    public void TrashAllocator(GameObject[] TrashList)
    {
        for (int i= 0;i<=TrashList.Length;i++)
        {
            if (TrashList[i].name[1] == 'g')
            {
                DataBase.g_waste[g_count].item = TrashList[i];
                DataBase.g_waste[g_count].name = TrashList[i].name;
            }

           else if (TrashList[i].name[1] =='r')
            {
                DataBase.r_waste[r_count].item = TrashList[i];
                DataBase.r_waste[r_count].name = TrashList[i].name;
            }
            else
            {
                Debug.Log("The asset does not fit any of the above category of the waste allocated or the naming of the asset is done in a wrong format");
            }
        }
    }
    
}
