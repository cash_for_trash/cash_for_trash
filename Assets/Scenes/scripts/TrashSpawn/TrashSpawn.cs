﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashSpawn : MonoBehaviour
{


    public GameObject[] Trashes;
    public Vector3 SpawnValues;
    public int TrashCount;
    public float spawnWait;
    public float startWait;
    public float WaveWait;

    private void Start()
    {
        StartCoroutine(SpawnTrash());
    }
    IEnumerator SpawnTrash()
    {
        yield return new WaitForSeconds(startWait*Time.deltaTime);
        while(true)
        {
            for(int i=0;i<TrashCount;i++)
            {
               
                GameObject Trash = Trashes[Random.Range(0, Trashes.Length)];
                Vector3 Position = new Vector3(SpawnValues.x,SpawnValues.y,SpawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(Trash, Position, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(WaveWait);
        }
    }
}
