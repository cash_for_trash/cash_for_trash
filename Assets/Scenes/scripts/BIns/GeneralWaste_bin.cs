﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralWaste_bin : MonoBehaviour
{
    public Score score;
    private void OnCollisionEnter(Collision collision)
    {
            if (score.score != 0)
            {
                score.score -= 5;
            }
            Destroy(collision.collider.gameObject);
    }
}
