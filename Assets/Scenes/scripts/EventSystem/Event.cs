﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Event 
{
    [System.Serializable]
    public class Vector2 : UnityEvent<Vector2> { }
}
