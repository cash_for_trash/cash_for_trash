﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ActiveItem :MonoBehaviour
{
    public Vector3 DragStopPos;
    public Vector3 DragStartPos;

    private Vector3 screenPoint;
    private Vector3 offset;

    public string Direction;
    public GameObject Trash;

    private Rigidbody rig;

    private Vector3 mOffset;

    private float mzCoord;

    //Physics trash;
    private void Start()
    {
      rig = Trash.GetComponent<Rigidbody>();

    }
    
    
        
    // public bool clickup=false;
    public bool click= false;

    private void OnMouseDown()
    {
        //screenPoint = Camera.main.WorldToScreenPoint(Input.mousePosition);

        mzCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
        mOffset = gameObject.transform.position - GetMouseWorldPos();

        DragStartPos = Input.mousePosition;
             rig.isKinematic = false;

       /* Debug.Log("StartPosition" + DragStartPos);*/
            click = true;
    }

    private Vector3 GetMouseWorldPos()

    {
        Vector3 mousePoint = Input.mousePosition;

        mousePoint.z = mzCoord;

        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    /*private void OnMouseUp()
     {
        DragStopPos = Input.mousePosition;
        //Debug.Log("StopPositon" + DragStopPos);
       // Direction = SwipeDricetion(DragStartPos, DragStopPos);
        
        //trash.TrashPopPhysics(Direction);

        click = false;
     }*/


    public void OnMouseDrag()
    {
        
       transform.position = GetMouseWorldPos()+mOffset;
       
    }

    /*
    public string SwipeDricetion(Vector3 DragStart,Vector3 DragStop)
    {
        string d="No Movement";
        if (DragStart.x>=DragStop.x)
        {
           d = "Left";
            
        }
        else if(DragStart.x<=DragStop.x)
        {
            d = "Right";
            
        }

        Debug.Log(d);


        return d;
    }*/
    
}
 