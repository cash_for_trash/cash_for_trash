﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_controller : MonoBehaviour
{
    public GameObject[] Trash;
    // Start is called before the first frame update
    void Start()
    {
        TrashAllocation allocator = gameObject.GetComponent<TrashAllocation>();
        allocator.TrashAllocator(Trash);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
